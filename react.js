const localRules = ['./rules/shared'].map(require.resolve)

module.exports = {
  extends: [
    'airbnb-typescript',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
    ...localRules,
  ],
  rules: {
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['to', 'linkProps'],
        aspects: ['noHref', 'invalidHref', 'preferButton'],
      },
    ],
    'react/prop-types': 'off',
    'react/require-default-props': 'off',
  },
}
