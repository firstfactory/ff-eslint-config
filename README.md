# Configuration using TypeScript, ESLint, Prettier with Visual Code integration

Set of rules and plugins for **ESLint** to be used in First Factory projects

## Usage

### Usage in React apps

Install the following dependencies

#### with npm

```bash
npm install eslint \
            @first-factory/eslint-prettier-config \
            @typescript-eslint/eslint-plugin@^2.34.0 \
            eslint-config-airbnb-typescript \
            eslint-config-prettier \
            eslint-plugin-import@^2.22.0 \
            eslint-plugin-jest \
            eslint-plugin-jsx-a11y@^6.3.1 \
            eslint-plugin-prettier \
            eslint-plugin-react@^7.20.5 \
            eslint-plugin-react-hooks@^2.5.1 \
            prettier \
            --save-dev
```

#### with yarn

```bash
yarn add -D eslint \
            @first-factory/eslint-prettier-config \
            @typescript-eslint/eslint-plugin@^2.34.0 \
            eslint-config-airbnb-typescript \
            eslint-config-prettier \
            eslint-plugin-import@^2.22.0 \
            eslint-plugin-jest \
            eslint-plugin-jsx-a11y@^6.3.1 \
            eslint-plugin-prettier \
            eslint-plugin-react@^7.20.5 \
            eslint-plugin-react-hooks@^2.5.1 \
            prettier \
```

Create a **.eslintrc** file with the following content:

```json
{
  "root":true,
  "extends": "./node_modules/@first-factory/eslint-prettier-config/react",
  "parserOptions": {
    "project": "./tsconfig.json",
    "createDefaultProgram": true
  }
}
```

Include the following scripts in your **package.json** file:

```json
    "lint": "eslint --resolve-plugins-relative-to ./ --ext .js,.ts,.jsx,.tsx ./",
    "lint:fix": "eslint --resolve-plugins-relative-to ./ --ext .js,.ts,.jsx,.tsx ./ --fix",
```

Create a **.prettierrc** file with this content:

```json
"./node_modules/@first-factory/eslint-prettier-config/.prettierrc.json"
```

### Usage in Node apps

Install the following dependencies

#### with npm

```bash
npm install eslint \
            prettier \
            @first-factory/eslint-prettier-config \
            eslint-config-airbnb-typescript \
            eslint-config-prettier \
            eslint-plugin-prettier \
            eslint-plugin-jest \
            eslint-plugin-import@^2.22.0 \
            @typescript-eslint/eslint-plugin@^2.34.0 \
            --save-dev
```

#### with yarn

```bash
yarn add -D eslint \
            prettier \
            @first-factory/eslint-prettier-config \
            eslint-config-airbnb-typescript \
            eslint-config-prettier \
            eslint-plugin-prettier \
            eslint-plugin-jest \
            eslint-plugin-import@^2.22.0 \
            @typescript-eslint/eslint-plugin@^2.34.0 \
```

Create a **.eslintrc** file with the following content:

```json
{
  "root":true,
  "extends": "./node_modules/@first-factory/eslint-prettier-config",
  "parserOptions": {
    "project": "./tsconfig.json",
    "createDefaultProgram": true
  }
}
```

Include the following scripts in your **package.json** file:

```json
    "lint": "eslint --resolve-plugins-relative-to ./ --ext .js,.ts ./",
    "lint:fix": "eslint --resolve-plugins-relative-to ./ --ext .js,.ts ./ --fix",
```

Create a **.prettierrc** file with this content:

```json
"./node_modules/@first-factory/eslint-prettier-config/.prettierrc.json"
```

## Integration with VSCode

In order to properly integrate ESLint and Prettier inside **VSCode** so that the
code is formatted automatically on the file after changes are done follow the
next steps:

1. Install
   [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
2. Install
   [Prettier extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
3. Apply this settings at **user** level in _VSCode_

```json
{
  "files.autoSave": "onFocusChange",
  "prettier.singleQuote": true,
  "prettier.requireConfig": true,
  "eslint.alwaysShowStatus": true,
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact"
  ],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
}
```

## Development

Include common rules to **React** and **NodeJS** applications inside
_./rules/shred.js_ file.
